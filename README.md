# Uber Interview Take Home assignment

This project was written by Joe Shih, for Uber Interview using.

## Initialize

Run `npm install` at first time, in order to download some package I used.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`.

## Function Introduction

This project is bae on [gfyCat](https://developers.gfycat.com/api/#introduction).

I made some features that user can interact with all the GIF in gfyCat.

1. Search GIF by keyword
2. Get Treading GIF
3. Get GIF by each Trending tag


