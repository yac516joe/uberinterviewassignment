import { RequestService } from '@/core/_services/request.service';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';


@Injectable()
export class HomeService {

    constructor(
        private requestService: RequestService
    ) { }

    login(data) {
        return this.requestService.Post(environment.baseUrl + '/oauth/token', data);
    }

    searchByKeyword(params?: { [param: string]: string | string[] }) {
        return this.requestService.Get(environment.baseUrl + '/gfycats/search', params);
    };
    searchByTrending(params?: { [param: string]: string | string[] }) {
        return this.requestService.Get(environment.baseUrl + '/reactions/populated', params);
    };
    getTrendingTags(params?: { [param: string]: string | string[] }){
        return this.requestService.Get(environment.baseUrl + '/tags/trending', params);
    }
    getGifByTagName(params?: { [param: string]: string | string[] }){
        return this.requestService.Get(environment.baseUrl + '/gfycats/trending', params);
    }
}