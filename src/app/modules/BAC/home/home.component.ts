import { Component } from '@angular/core';
import { HomeService } from './home.service';
import { gfycat_Search } from '@/models/gfycat_Search';
import { SelectedItemService } from '@/core/_services/selectedItem.service';
import { SelectItem } from 'primeng/api';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { gfycat_Content } from '@/models/gfycat_Content';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

  private searchModel: gfycat_Search;
  private searchResult: gfycat_Content[];
  private trendingResult: gfycat_Content[];
  private trendingTags: string[];
  private getByTagNameResult: gfycat_Content[];

  constructor(
    private homeService: HomeService,
    private fb: FormBuilder
  ) {

  }

  ngOnInit() {
    this.searchModel = new gfycat_Search();
    this.resetResult();

    if(this.isLogin()){
      this.getTrendingTags();
    }
  }

  //Button Event
  login() {
    var loginCredential = {
      "grant_type": "client_credentials",
      "client_id": "2_lm_ytg",
      "client_secret": "VtNswBklgA1Sw_tOVwYt0XmloOy8A2_x6EKxQ78FD3hqOdGMaILZXu7h0g4QUU6K"
    };
    this.homeService.login(loginCredential)
      .subscribe(result => this.loginSucceed(result));

  }
  searchByKeyword() {
    this.resetResult();
    var param = { search_text: this.searchModel.keyword };
    this.homeService.searchByKeyword(param)
      .subscribe(result => this.searchByKeywordSucceed(result));
  }
  searchByTrending() {
    this.resetResult();
    var param = { tagName: "trending" };
    this.homeService.searchByTrending(param)
      .subscribe(result => this.searchByTrendingSucceed(result));
  }
  getGifByTagName(tagName: string) {
    this.resetResult();
    var param = { tagName: tagName };
    this.homeService.getGifByTagName(param)
      .subscribe(result => this.getGifByTagNameSucceed(result));

  }
  //Success Callback
  loginSucceed(result) {
    if (!result || !result.access_token || !result.expires_in) {
      return;
    }
    var token = result.access_token;

    localStorage.setItem('currentUser', token);
    this.logoutTimer(result.expires_in);
    this.getTrendingTags();
  }
  searchByKeywordSucceed(result) {
    if (!result || !result.cursor) {
      return;
    }

    this.searchResult = <gfycat_Content[]>result.gfycats;
  }
  searchByTrendingSucceed(result) {
    if (!result || !result.cursor) {
      return;
    }

    this.trendingResult = <gfycat_Content[]>result.gfycats;
  }
  getTrendingTagsSucceed(result) {
    if (!result || result.length == 0) {
      return;
    }

    this.trendingTags = <string[]>result;
    this.trendingTags = this.trendingTags.slice(0, 10);
  }
  getGifByTagNameSucceed(result) {
    this.getByTagNameResult = [];
    if (!result || !result.cursor) {
      return;
    }

    this.getByTagNameResult = <gfycat_Content[]>result.gfycats;
  }


  //Init
  getTrendingTags() {
    var param = {};
    this.homeService.getTrendingTags(param)
      .subscribe(result => this.getTrendingTagsSucceed(result));
  }
  resetResult() {
    this.searchResult = [];
    this.trendingResult = [];
    this.getByTagNameResult = [];
  }

  //Other
  logoutTimer(expiredSec: number) {
    var interval = setInterval(() => {
      this.logout();
      clearInterval(interval);
    }, expiredSec * 1000)

  }
  logout() {
    localStorage.removeItem('currentUser');
  }
  isLogin() {
    return localStorage.getItem('currentUser');
  }

}
