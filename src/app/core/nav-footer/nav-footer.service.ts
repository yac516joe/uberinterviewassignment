import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { RequestService } from '../_services/request.service';

@Injectable()
export class NavFooterService {

    constructor(
        private requestService: RequestService
    ) { }

}