import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { NavMenuService } from '../nav-menu/nav-menu.service';


@Component({
    selector: 'app-nav-header',
    templateUrl: './nav-header.component.html',
    styleUrls: ['./nav-header.component.css']
})
export class NavHeaderComponent implements OnInit {

    menus: MenuItem[];
    showSidebar: boolean = false;

    constructor(
        private router: Router,
        private navMenu: NavMenuService,
    ) {

    }

    ngOnInit() {
        var that = this;

        this.navMenu
            .subscribeSelectMenu()
            .subscribe(() =>
                this.showSidebar = false
            );

    }
    getNavMenu() {
        this.navMenu
            .subscribeMenus()
            .subscribe((data) => {
                this.menus = data;
            });
    }
    logout() {
        this.router.navigate(['login']);
        this.showSidebar = false;
    }
}
