﻿import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { LoaderService } from '../../share/_components/loader.service';
import { finalize, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Injectable()
export class CusomehttpInterceptor implements HttpInterceptor {
    constructor(
        public loaderService: LoaderService,
        private messageService: MessageService,
        private router: Router,
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({
            setHeaders: {
                "Accept": "application/json"
            },
        });

        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
                if (error.status == 401) {
                    localStorage.removeItem('currentUser');
                }

                this.handleError(error);
                return of(null);
            }),
            finalize(() => this.loaderService.finish())
        );
    }

    handleError(error: HttpErrorResponse) {
        this.loaderService.finish();
        this.messageService.add({ severity: 'error', summary: '失敗', detail: error.statusText });
    }
}