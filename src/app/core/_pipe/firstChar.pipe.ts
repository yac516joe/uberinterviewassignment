import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'firstChar'})
export class FirstCharPipe implements PipeTransform {
  transform(value: string): string {
      if(value){
          return value.substr(0,1);
      }else{
          return "";
      }
  }
}