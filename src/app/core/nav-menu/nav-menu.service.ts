import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { MenuItem } from 'primeng/api';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { RequestService } from '../_services/request.service';


@Injectable()
export class NavMenuService {

    public menus: MenuItem[];
    private menus_subject: BehaviorSubject<MenuItem[]>;
    private selectMenu_subject: BehaviorSubject<MenuItem>;

    constructor(
        private requestService: RequestService,
        private router: Router) {
        this.menus_subject = new BehaviorSubject<MenuItem[]>([]);
        this.selectMenu_subject = new BehaviorSubject<MenuItem>({});
        this.getMenu();
    }
    getMenu() {
       
    };
    subscribeMenus(): Observable<MenuItem[]> {
        return this.menus_subject.asObservable();
    }

    subscribeSelectMenu(): Observable<MenuItem> {
        return this.selectMenu_subject.asObservable();
    }

    GetChildMenu(rawMenu, parentNo: number): MenuItem[] {
        var that = this;
        function menuClick(event) {
            if (!event.item.items) {
                that.selectMenu_subject.next(event.item);
            }
        }

        let result = [];
        let parentNodes = rawMenu.filter(function (element) {
            return element.ParentNO == parentNo;
        });

        parentNodes.forEach(parent => {
            let childMenu = { label: "", routerLink: "", items: null, expanded: false, command: {} };
            if (parent.IsParent == "Y") {
                childMenu.items = this.GetChildMenu(rawMenu, parent.MenuNO);
            }

            if (parent.ParentNO == 0) {
                childMenu.expanded = true;
            }
            childMenu.label = parent.MenuNM;
            childMenu.routerLink = parent.Route;
            childMenu.command = menuClick;

            result.push(childMenu);
        })

        return result;
    }
    getSelectedMenu() {
        var subPath = this.router.url;
    }
}