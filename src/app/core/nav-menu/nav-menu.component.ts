import { Component, HostListener, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { NavMenuService } from './nav-menu.service';

@HostListener('window:resize', ['$event'])
@Component({
    selector: 'app-nav-menu',
    templateUrl: './nav-menu.component.html',
    styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
    isExpanded = false;
    isHidden = false;
    public menus: MenuItem[];
    public versionNum: string;
    public menuHeight: number;
    constructor(
        private menuService: NavMenuService,
    ) {
        this.onResize();
    }

    ngOnInit() {
    }
    getMenu() {
        var that = this;

        this.menuService.getMenu();
        this.menuService
            .subscribeMenus()
            .subscribe(data => that.menus = data);
    }
    getVersion() {
        var that = this;
    }

    logout() {
    }
    onResize(event?) {
        this.menuHeight = window.innerHeight - 100;
    }
    hasMenu() {
        return this.menus.length > 0;
    }
    isLogin() {
    }
    hideMenu() {
        this.isHidden = true;
    }
    showMenu() {
        this.isHidden = false;
    }
}
