import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';


@Injectable()
export class PrintService {

    parentStyle: any;
    pageStyle: any;
    iframeEle: any;

    documnet: any;
    constructor(@Inject(DOCUMENT) _document) {
        this.documnet = _document;
    }

    print(elementId: string): void {
        this.getParentStyle();
        this.setPageStyle();
        this.createIframe();
        this.addContent(elementId);
        this.printIframe();
    }
    private printIframe() {
        var that = this;

        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            document.body.removeChild(that.iframeEle);
        }, 500);
    }
    private addContent(elementId: string) {
        var contents = document.getElementById(elementId).innerHTML;
        document.body.appendChild(this.iframeEle);
        var frameDoc = this.iframeEle.contentWindow;
        frameDoc.document.open();
        frameDoc.document.write('<html><head><title></title>');
        frameDoc.document.write(this.parentStyle);
        frameDoc.document.write(this.pageStyle);
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
    }
    private createIframe() {
        this.iframeEle = document.createElement('iframe');

        this.iframeEle.name = "frame1";
        this.iframeEle.style.position = "absolute";
        this.iframeEle.style.width = "1000px";
        this.iframeEle.style.top = "-1000000px";

    }
    private getParentStyle() {
        var styles = this.getElementTag('style');
        var links = this.getElementTag('link');

        this.parentStyle = styles + links;
    }
    private setPageStyle() {
        this.pageStyle =
            "<style>" +
            "@media print {" +
            "@page {margin:0 5mm 0 0;size: 5.7cm 10cm;}" +
            "html {margin:0px;}" +
            "body {min-width:5.7cm!important;font-size:12px;margin:0px;}" +
            "}" +
            "</style>";
    }
    private getElementTag(tag: keyof HTMLElementTagNameMap): string {
        const html: string[] = [];
        const elements = document.getElementsByTagName(tag);
        for (let index = 0; index < elements.length; index++) {
            html.push(elements[index].outerHTML);
        }
        return html.join('\r\n');
    }
}