import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoaderService } from '@/share/_components/loader.service';

@Injectable()
export class RequestService {
    constructor(
        private http: HttpClient,
        private loaderService: LoaderService,
    ) { }

    public Post<T>(url: string, value: any, loading = true) {
        let currentUser = localStorage.getItem('currentUser');
        if (loading) {
            this.loaderService.start();
        }
        let options = {
            headers: {
                'Authorization': `Bearer ${currentUser}`,
            }
        };


        return this.http.post<T>(url, value, options);
    }
    public Get<T>(url: string, params?: { [param: string]: string | string[] }) {
        let currentUser = localStorage.getItem('currentUser');
        let options = {
            headers: {
                'Authorization': `Bearer ${currentUser}`,
            },
            params: params
        };

        return this.http.get<T>(url, options);
    }
    public DownloadFile(path, loading = true) {
        let currentUser = localStorage.getItem('currentUser');
        let options = {
            responseType: 'blob' as 'json',
            header: {
                'Authorization': `Bearer ${currentUser}`,
            }
        };
        if (loading) {
            this.loaderService.start();
        }

        return this.http.get(path, options);
    }

    public DownloadFileByPost(path, value, loading = true) {
        let currentUser = localStorage.getItem('currentUser');
        let options = {
            responseType: 'blob' as 'json',
            header: {
                'Authorization': `Bearer ${currentUser}`,
            }
        };
        if (loading) {
            this.loaderService.start();
        }

        return this.http.post(path, value, options);
    }

    public PostForm(path, value) {
        let options = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            }
        };

        return this.http.post(path, value, options);
    }
}