import { Injectable } from '@angular/core';
import * as xlsx from "xlsx";
import * as FileSaver from "file-saver";
import { DatePipe } from '@angular/common';

@Injectable()
export class ExportService {
    constructor(
        private datepipe: DatePipe
    ) { }

    public ToExcel(headers: string[][], content, filename:string) {

        var ws = xlsx.utils.aoa_to_sheet(headers);
        ws = xlsx.utils.sheet_add_json(ws, content, { skipHeader: true, origin: "A2" });
        const workbook = { Sheets: { 'data': ws }, SheetNames: ['data'] };
        const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, filename);
    }

    public saveAsExcelFile(buffer: any, fileName: string): void {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + "_" + this.datepipe.transform(new Date(), 'yyyyMMdd') + EXCEL_EXTENSION);
    }
}