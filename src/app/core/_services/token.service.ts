import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
export const TOKEN = 'currentUser';

@Injectable()
export class TokenService {
    constructor(
        private jwtHelper: JwtHelperService
    ) { }

    isTokenExpired(token: string = TOKEN): boolean {
        let tokenStr = this.getToken(token);

        if (tokenStr) {
            return this.jwtHelper.isTokenExpired(tokenStr);  // token expired?
        } else {
            return true;        // no token 
        }
    }

    decodeToken(key: string) {
        let tokenStr = this.getToken(key);

        if(this.isTokenExpired(key)){
            return null;
        }else{
            return this.jwtHelper.decodeToken(tokenStr);
        }
    }

    writeToken(key: string, token: string = TOKEN) {
        localStorage.setItem(key, token);
    }

    getToken(token: string = TOKEN) {
        return localStorage.getItem(token);
    }

    removeToken(token: string = TOKEN) {
        if (this.getToken(token)) {
            localStorage.removeItem(token);
        }
    }
}