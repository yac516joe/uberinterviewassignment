﻿import { Injectable } from '@angular/core';
import { BrowserXhr } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { MessageService } from 'primeng/api';
import { LoaderService } from '../../share/_components/loader.service';

@Injectable()
export class UploadService {
    progress$: Observable<any>;
    progressObserver: any;
    progress: number;
    constructor(
        private browserXHR: BrowserXhr,
        private messageService: MessageService,
        private loaderService: LoaderService,
    ) {
        this.progress$ = Observable
            .create(observer => {
                this.progressObserver = observer;
            })
            .share();
    }

    makeFileRequest(url: string, params: any, files: File[]):
        Observable<any> {
        return Observable.create(observer => {
            const formData: FormData = new FormData();
            const xhr: XMLHttpRequest = this.browserXHR.build();
            this.loaderService.start();

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    let res = JSON.parse(xhr.response);

                    if (xhr.status === 200) {
                        observer.next(res);
                    } else {
                        this.handleError(res)
                        observer.error(res);
                    }
                    this.loaderService.finish();
                }
            };

            //xhr.upload.onprogress = (event) => {
            //    this.progress = Math.round(event.loaded / event.total * 100);

            //    this.progressObserver.next(this.progress);
            //};
            if (files) {
                for (let i = 0; i < files.length; i++) {
                    formData.append('upload', files[i], files[i].name);
                }
            }
            this.toFormData(formData, params, "");

            xhr.open('POST', url, true);
            this.SetHeader(xhr);
            xhr.send(formData);
        });
    }
    handleError(error: string) {
        this.messageService.add({ severity: 'error', summary: '失敗', detail: error });
    }

    SetHeader(xhr) {
        let currentUser = localStorage.getItem('currentUser');

        if (currentUser) {
            xhr.setRequestHeader('Authorization', `Bearer ${currentUser}`);
        }
    }
    toFormData(formData, data, path) {
        if (!isObject(data)) {
            return;
        }

        var p;

        for (p in data) {
            if (!data.hasOwnProperty(p)) {
                continue;
            }

            var appendFn;
            var propVal = data[p];

            if (isFileList(propVal) || (Array.isArray(propVal) && !isArrayOfObject(propVal))) {
                appendFn = appendArray;
            } else if (isArrayOfObject(propVal)) {
                for (var i = 0; i < propVal.length; i++) {
                    this.toFormData(formData, propVal[i], p + "[" + i + "].");
                }
                continue;
            } else if (isObject(propVal)) {
                this.toFormData(formData, propVal, p + ".");
            } else {
                appendFn = append;
            }

            if (appendFn) {
                appendFn(p, propVal);
            }
        }

        function isFileList(data) {
            return isObject(data) && Array.isArray(data) && data.length > 0 && data[0] instanceof File;
        }

        function isArrayOfObject(data) {
            var result = false;

            if (Array.isArray(data)) {
                for (var i = 0; i < data.length; i++) {
                    if (isObject(data[i])) {
                        result = true;
                        break;
                    }
                }
            }

            return result;
        }

        function appendArray(name, arrayVal) {
            var arrayName = name;
            if (!arrayVal) {
                return;
            }
            for (var i = 0; i < arrayVal.length; i++) {
                append(arrayName, arrayVal[i]);
            }
        }

        function append(name, value) {
            if (value != null) {
                formData.append(path + name, value);
            }
        }

        function isObject(data) {
            return typeof data === 'object';
        }
    }
}