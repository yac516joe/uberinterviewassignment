import { Injectable } from '@angular/core';
import * as xlsx from "xlsx";
import * as FileSaver from "file-saver";
import { DatePipe } from '@angular/common';

@Injectable()
export class DateService {
    constructor(
    ) { }

    public DateToString(date: Date, format: string) {
        var dd = String(date.getDate()).padStart(2, "0");
        var mm = String(date.getMonth() + 1).padStart(2, "0");
        var yyyy = date.getFullYear();
        var hh = String(date.getHours()).padStart(2, "0");
        var minute = String(date.getMinutes()).padStart(2, "0");
        var second = String(date.getSeconds()).padStart(2, "0");

        if (format == "yyyy-mm-dd") {
            return yyyy + '-' + mm + '-' + dd;
        } else if (format == "yyyy-mm") {
            return yyyy + '-' + mm;
        } else if (format == "yyyy/mm/dd") {
            return yyyy + '/' + mm + '/' + dd;
        } else if (format == "hh:mm") {
            return hh + ":" + minute;
        } else if (format == "hh:mm:ss") {
            return hh + ":" + minute + ":" + second;
        }
    }
    public StringToDate(dateStr) {
        var date = new Date(dateStr);

        if (isNaN(date.getTime())) {
            date = new Date(dateStr.replace(/ /g, "T"));
        }

        return date;
    }
    public GetToday(dateFormat = "yyyy-mm-dd") {
        var today = new Date();

        return this.DateToString(today, dateFormat);
    }
    public GetNowTime() {
        var today = new Date();

        return this.DateToString(today, "hh:mm");
    }
    public AddDays(dateStr: string, addDays: number): string {
        var date = new Date(dateStr);
        date.setDate(date.getDate() + addDays);

        return this.DateToString(date, "yyyy-mm-dd");
    }
    public AddMinutes(hhmm: string, addMinutes: number): string {
        var hour = hhmm.substring(0, 2);
        var minute = hhmm.substring(3, 5);

        var rawMinute = addMinutes + Number(minute);
        var addHours = Math.floor(rawMinute / 60);
        if (rawMinute >= 0) {
            var lastMinute = rawMinute % 60;
        } else  {
            var lastMinute = rawMinute + 60;
        }

        return String(Number(hour) + addHours) + ":" + String(lastMinute).padStart(2, "0");
    }
    public MinuteDiff(from: string, to: string): number {
        var hourFrom = Number(from.substring(0, 2));
        var hourTo = Number(to.substring(0, 2));
        var minuteFrom = Number(from.substring(3, 5));
        var minuteTo = Number(to.substring(3, 5));

        return ((hourTo - hourFrom) * 60) + (minuteTo - minuteFrom);
    }
}