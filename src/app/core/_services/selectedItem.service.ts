import { Injectable } from '@angular/core';
import { SelectItemGroup } from 'primeng/api';

@Injectable()
export class SelectedItemService {
    constructor(
    ) { }

    convertToSelectItem(value: any[], valueProperty: string, labelProperty: string) {
        if (value && value.length > 0) {
            return value.map(function (item) {
                return {
                    value: item[valueProperty].toString(),
                    label: item[labelProperty].toString()
                };
            });
        } else {
            return [{
                value: null,
                label: "請選擇"
            }];
        }
    }
    convertToSelectItem_Custom(value: any[], valueProperty: string, labelProperties: string[]) {
        var that = this;

        if (value && value.length > 0) {
            return value.map(function (item) {
                return {
                    value: item[valueProperty],
                    label: that.combineProperty(item, labelProperties)
                };
            });
        } else {
            return [{
                value: null,
                label: "請選擇"
            }];
        }
    }

    convertToSelectItemGroup(value: any[], valueProperty: string, labelProperty: string, ParentFlag: string, ParentProperty: string) {
        if (value && value.length > 0) {
            let result: SelectItemGroup[] = [];

            value.filter(x => x[ParentFlag] == "Y")
                .forEach(function (x) {
                    let tmp: SelectItemGroup = { label: "", items: [] };

                    tmp.label = x[labelProperty];
                    tmp.items = value.filter(y => y[ParentProperty] == x[valueProperty])
                        .map(function (item) {
                            return {
                                value: item[valueProperty],
                                label: item[labelProperty].toString()
                            };

                        })

                    result.push(tmp);
                });

            return result;
        } else {
            return null;
        }
    }
    combineProperty(item: any, properties: string[]) {
        let result: string[] = [];

        properties.forEach(
            val => result.push(item[val])
        );

        return result.join(" - ");
    }
}