import { Injectable } from '@angular/core';
import { SelectItem } from 'primeng/api';

@Injectable()
export class OptionListService {
    constructor(
    ) { }

    GenServiceTimeList(start: number, end: number) {
        let serviceTimeList: SelectItem[] = [];
        let zero: SelectItem;
        let thirty: SelectItem;

        for (let i = 0; start + i < end; i++) {
            var hours = String(start + i).padStart(2, "0");
            zero = { label: hours + ":00", value: hours + ":00" };
            thirty = { label: hours + ":30", value: hours + ":30" };

            serviceTimeList.push(zero);
            serviceTimeList.push(thirty);
        }

        zero = { label: end + ":00", value: end + ":00" };
        serviceTimeList.push(zero);

        return serviceTimeList;
    }

    ExpandBetweenHHMM(start: string, end: string): SelectItem[] {
        var result: SelectItem[] = [];
        if (!start) {
            return [];
        } else if (!end) {
            end = start;
        }

        start = this.CorrectTime(start);
        end = this.CorrectTime(end);

        for (let tmp = start; tmp < end; tmp = this.NextPeriod(tmp)) {
            result.push({ label: tmp, value: tmp })
        }

        return result
    }
    NextNPeriod(hhmm: string, periodCount = 1) {
        if (!hhmm) {
            return;
        }

        var result = hhmm;
        for (let index = 0; index < periodCount; index++) {
            result = this.NextPeriod(result);
        }

        return result;
    }
    NextPeriod(hhmm: string) {
        var hour = hhmm.substring(0, 2);
        var minute = hhmm.substring(3, 5);

        if (minute >= "45" && minute < "60") {
            return String(Number(hour) + 1).padStart(2, "0") + ":00";
        } else if (minute >= "30" && minute < "45") {
            return hour + ":45";
        } else if (minute >= "15" && minute < "30") {
            return hour + ":30";
        } else if (minute >= "00" && minute < "15") {
            return hour + ":15";
        }
    }
    CorrectTime(hhmm: string) {
        var hour = hhmm.substring(0, 2);
        var minute = hhmm.substring(3, 5);

        if (!hour || !minute) {
            return "";
        }

        if (minute == "00") {
            return hour + ":00";
        } else if (minute <= "15") {
            return hour + ":15";
        } else if (minute <= "30") {
            return hour + ":30";
        } else if (minute <= "45") {
            return hour + ":45";
        } else {
            return String(Number(hour) + 1).padStart(2, "0") + ":00";
        }
    }
}