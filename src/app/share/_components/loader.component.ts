﻿import { Component, OnInit } from '@angular/core';
import { LoaderService } from './loader.service';

@Component({
    selector: 'erp-loader',
    templateUrl: 'loader.component.html',
    styleUrls: ['./loader.component.css']
})

export class LoaderComponent implements OnInit {
    isLoading = false;
    private loaderService: LoaderService;
    constructor(loaderService: LoaderService) {
        this.loaderService = loaderService;
    }

    ngOnInit() {
        this.loaderService.loader.subscribe(value => this.isLoading = value)
    }

    showLoading() {
        return this.isLoading;
    }
}