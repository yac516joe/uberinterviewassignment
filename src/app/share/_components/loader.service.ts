﻿import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LoaderService {
    private isLoading = false;
    public loader;

    constructor() {
        this.loader = new BehaviorSubject(this.isLoading)
    }

    start() {
        this.isLoading = true;
        this.loader.next(this.isLoading)
    }

    finish() {
        this.isLoading = false;
        this.loader.next(this.isLoading)
    }
}