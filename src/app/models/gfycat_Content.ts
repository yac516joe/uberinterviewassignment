﻿
export class gfycat_Content {
    gifUrl: string;
    gif100px:string;
    title:string;
    
    public constructor(init?:Partial<gfycat_Content>) {
        Object.assign(this, init);
    }
}
