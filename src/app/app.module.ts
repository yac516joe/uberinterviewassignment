import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtModule } from '@auth0/angular-jwt';

import { AppComponent } from './app.component';

import { NavMenuComponent } from './core/nav-menu/nav-menu.component';
import { NavHeaderComponent } from './core/nav-header/nav-header.component';
import { NavFooterComponent } from './core/nav-footer/nav-footer.component';
import { AuthGuard } from './core/_guards';
import { CusomehttpInterceptor } from './core/_helpers/Cusomehttp.interceptor';
import { TokenInterceptor } from './core/_helpers/token.interceptor';
import { SelectedItemService } from './core/_services/selectedItem.service';
import { UploadService } from './core/_services/upload.service';
import { ExportService } from './core/_services/export.service';
import { DateService } from './core/_services/date.service';
import { RequestService } from './core/_services/request.service';
import { PrintService } from './core/_services/print.service';

import { LoaderComponent } from './share/_components';

import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { PanelMenuModule } from 'primeng/panelmenu';
import { SidebarModule } from 'primeng/sidebar';

import { HomeComponent } from './modules/BAC/home/home.component';
import { HomeService } from './modules/BAC/home/home.service';
import { NavMenuService } from './core/nav-menu/nav-menu.service';
import { NavFooterService } from './core/nav-footer/nav-footer.service';

export function tokenGetter() {
    return localStorage.getItem('currentUser');
}

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        NavHeaderComponent,
        NavFooterComponent,
        HomeComponent,
        LoaderComponent,
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        CardModule,
        ButtonModule,
        InputTextModule,
        MessagesModule,
        MessageModule,
        DialogModule,
        BrowserAnimationsModule,
        DropdownModule,
        ToastModule,
        ConfirmDialogModule,
        ProgressSpinnerModule,
        PanelMenuModule,
        SidebarModule,
        RouterModule.forRoot([
            { path: '', component: HomeComponent, pathMatch: 'full', canActivate: [AuthGuard] },
        ], { useHash: true, anchorScrolling: 'enabled' }),
        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetter
            }
        })
    ],
    providers: [
        AuthGuard,
        SelectedItemService,
        UploadService,
        ExportService,
        DateService,
        HomeService,
        RequestService,
        PrintService,
        ConfirmationService,
        NavMenuService,
        MessageService,
        NavFooterService,
        { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: CusomehttpInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
